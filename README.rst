=======
glvault
=======

A secrets vault using Gitlab CI/CD variables set as backend, usable from Python apps and shell.

Installation
============

``glvault`` requires Python 3.6 (and perhaps works with later Python versions) and will work on any platform.

.. code:: console

   pip install glvault

If you have a read access to the repository, you may try this too to get the bleeding edge version:

.. code:: console

   pip install git+https://http://repo-48a.westus2.cloudapp.azure.com/billy/glvault.git

.. note::

   ``glvault`` does not need extra requirements. All is in stdlib.

Shell usage
===========

Create a Gitlab project as a vault
----------------------------------

You may either use an existing project or create a dedicated project to store and share project secrets in its CI/CD
variables.

You may prefer a group CI/CD variables set as backend if you want to share these variables with the group members.

Global options and environment variables
----------------------------------------

``--endpoint`` / ``-e`` option and ``GLVAULT_ENDPOINT`` environment variable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As above written, `glvault` uses a Gitlab backend. Thus it needs its API base URL. If your backend is a group this URL
is in the form:

.. code:: text

   <home_url>/groups/<group_id>

If your project is elsewhere (i.e. a personal project); this URL is in the form:

.. code:: text

   <home_url>/projects/<project_id>

- ``<home_url>`` is the home URL of your Gitlab site

- ``<group_id>`` is a number displayed in the "Settings > General" form of a group.

- ``<project_id>`` is a number displayed under the title of the home page of a project.

Examples:

- https://gitlab.mycompany.io/projects/43
- https://gitlab.mycompany.io/groups/12

You **must** provide this this URL either to the ``--endpoint`` / ``-e`` option of the ``glvault`` command or through
the ``GLVAULT_ENDPOINT`` environment variable.

Note that the command-line option has the highest priority that let you override the environment variable.

``--api-token`` / ``-t`` option and ``GLVAULT_API_TOKEN`` environment variable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You need a Gitlab key suitable to API to have the read/write access granted to the considered CI/CD variables backend.

If you can read and change CI/CD variables of the group or project used as backend from your browser with your usual
credentials, you can create an API key.

Browse to "Settings > Access tokens", name a new token and check the "api" option. Get the newly generated token and
paste it in a safe place.

You authenticate your ``glvault`` usages with this key, either with the ``--api-token`` / ``-t`` option or with the
``GLVAULT environment variable.

You **must** provide a valid token by whatever means you prefer, otherwise ``glvauld`` won't work.

Note that the command-line option has the highest priority that let you override the environment variable.

``--human`` / ``-h`` option
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The usual output of a command to stdout is in its simplest form such it is easily parse-able by usual shell script
utilities (``awk`` ...). This option provides the result in a more verbose and human readable form.

``--json`` / ``-j`` option
~~~~~~~~~~~~~~~~~~~~~~~~~~

This option prints out the result of the command such it can be parsed by the JSON library by a custom tool built in any
language that can read JSON.

Subcommands
-----------

Listing existing variables
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   glvault list

These specific options are available:

- ``--names``: show only the variable names
- ``--verbose``: shows the "protected" status too

Adding a variable
~~~~~~~~~~~~~~~~~

.. code:: bash

   glvault create [-p|--protect] <name> <value>

The specific option are available:

If there is one or more space in the value, surround it with quotes (") or apostrophes (').

``-p`` / ``--protect`` makes a protected variable.

Changing a variable
~~~~~~~~~~~~~~~~~~~

.. code:: bash

   glvault update <name> <value>

If there is one or more space in the value, surround it with quotes (") or apostrophes (').

Getting a variable
~~~~~~~~~~~~~~~~~~

.. code:: bash

   glvault get <name>

Deleting a variable
~~~~~~~~~~~~~~~~~~~

.. code:: bash

   glvault rm <name>

Python usage
============

All happens around a configured ``GLVault`` instance with methods ``list()``, ``add()``, ``update()``, ``get()``, ``delete()``.

.. code:: python

   import glvault
   # ...
   myvault = glvault.GLVault("https://my-gitlab.io/projects/13", "xxx-gitlab-api-token-xxx)

   # List all variables with defaults
   myvault.list(show_values=False, show_protected=False)  # same as myvault.list()
   # -> [{"name": "SOME_VARIABLE"}, {...}, ...]

   # List all variables with values and protection status
   myvault.list(show_values=True, show_protected=True)
   # -> [{"name": "SOME_VARIABLE", "value": "10.2.1.6", "protected": False}, ...]

   # Adding a variable
   # myvault.add(name, value, protected=False)
   myvault.add("NEW_VARIABLE", "value", protected=True)

   # Note that the ``value`` is always converted to ``str`` and ``protected`` to ``bool``.

Background
==========

- Gitlab API: https://docs.gitlab.com/ee/api/
- Project level variables: https://docs.gitlab.com/ee/api/project_level_variables.html
- Group level variables: https://docs.gitlab.com/ee/api/group_level_variables.html

Developer notes
===============

Gitlab home
-----------

https://gitlab.com/gilles.lenfant/glvault

Running unit tests
------------------

Provide environment variables for the tests project or group backend (``GLVAULT_ENDPOINT``) and an API key granted for
playing with the variables (``GLVAULT_API_TOKEN``) of this project or group as above described.

You may use your fork of this project for this. Be warned that the unit tests will build variables prefixed with "GLVT_"
and delete them after the tests.

.. code:: console

   python -m unittest tests
