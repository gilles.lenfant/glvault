"""
===============
glvault package
===============
"""
import pathlib
from setuptools import setup

version = "1.0.0"

this_directory = pathlib.Path(__file__).resolve().parent

setup(
    name="glvault",
    version=version,
    description="A shared vault with Gitlab backend",
    long_description=(this_directory / "README.rst").read_text().strip(),
    py_modules=["glvault"],
    entry_points={
        "console_scripts": ["glvault=glvault:main"]
    }
)
