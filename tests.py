"""
Unit tests for glvault
"""
import os
import shlex
import types
import unittest
import glvault

ENDPOINT = os.getenv("GLVAULT_ENDPOINT",
                     "https://gitlab.com/projects/12157105")
TOKEN = os.getenv("GLVAULT_API_TOKEN", "wnAex5EPtDN71_XxweZE")
TEST_VAR_PREFIX = "_GLVT_"


@unittest.skip
class CommandParsingTest(unittest.TestCase):
    def setUp(self):
        self.parser = glvault.make_arg_parser()

    def test_no_endpoint(self):
        with self.assertRaises(SystemExit):
            opts = self.parser.parse_args(["-e"])

    def test_invalid_endpoint(self):
        with self.assertRaises(SystemError):
            args = shlex.split("-e http://gitlab")
            opts = self.parser.parse_args(["-e", "http://gitlab"])

    def test_valid_endpoint(self):
        opts = self.parser.parse_args(["-e", "http://gitlab/projects/15"])

    def test_two(self):
        self.assertEqual(0, 0)


def make_varname(name: str) -> str:
    return TEST_VAR_PREFIX + name.strip()

# Warning: this is a monolithic test. Do not execute test methods separately from others.


class GLVaultTest(unittest.TestCase):
    def setUp(self):
        self.vault = glvault.GLVault(ENDPOINT, TOKEN)
        self._cleanup_backend()

    def tearDown(self):
        self._cleanup_backend()

    def _cleanup_backend(self):
        test_names = (var_info["key"] for var_info in self.vault.list(
        ) if var_info["key"].startswith(TEST_VAR_PREFIX))
        for name in test_names:
            self.vault.remove(name)

    def test_setup(self):
        expect = "https://gitlab.com/api/v4/projects/12157105/"
        self.assertEqual(self.vault.api_base, expect)

    def test_list(self):
        result = self.vault.list()
        self.assertIsInstance(result, list)

    def test_show(self):
        result = self.vault.show("SOUTH")
        self.assertIsInstance(result, dict)

    def test_new(self):
        result = self.vault.new("WEST", "east")
        self.assertIsInstance(result, dict)

    def test_update(self):
        result = self.vault.update("WEST", "west")
        self.assertIsInstance(result, dict)

    def test_remove(self):
        result = self.vault.remove("WEST")
        self.assertIsInstance(result, dict)


class LinkParseTest(unittest.TestCase):

    def test_page_link_re(self):
        link = '<http://somewhere?query=string>; rel="top"'
        result = glvault.page_link_re.match(link)
        self.assertEqual(result.group(1), "http://somewhere?query=string")
        self.assertEqual(result.group(2), "top")

    def test_parse_pagination(self):
        headers = {"Link": '<https://gitlab.example.com/api/v4/projects/8/issues/8/notes?page=1&per_page=3>; rel="prev", '
                           '<https://gitlab.example.com/api/v4/projects/8/issues/8/notes?page=3&per_page=3>; rel="next", '
                           '<https://gitlab.example.com/api/v4/projects/8/issues/8/notes?page=1&per_page=3>; rel="first", '
                           '<https://gitlab.example.com/api/v4/projects/8/issues/8/notes?page=3&per_page=3>; rel="last"'}
        expected = {
            "prev": "https://gitlab.example.com/api/v4/projects/8/issues/8/notes?page=1&per_page=3",
            "next": "https://gitlab.example.com/api/v4/projects/8/issues/8/notes?page=3&per_page=3",
            "first": "https://gitlab.example.com/api/v4/projects/8/issues/8/notes?page=1&per_page=3",
            "last": "https://gitlab.example.com/api/v4/projects/8/issues/8/notes?page=3&per_page=3"
        }
        response = types.SimpleNamespace(headers=headers)
        result = glvault.parse_pagination(response)
        self.assertEqual(result, expected)
