"""
==============
glvault module
==============

A vault (secrets and passwords sore) with Gitlab CI/CD variables as backend.
"""
import abc
import argparse
import enum
import functools
import json
import logging
import os
import re
import sys
from urllib.parse import urlparse, urlunparse, urljoin, urlencode
from urllib.request import Request, urlopen
from urllib.error import HTTPError

__description__ = "A collective vault with Gitlab project or group variables backend"
__version__ = "1.0.0"

UNSET_OPTION = "__unset__"
GLVAULT_ENDPOINT = os.getenv("GLVAULT_ENDPOINT", UNSET_OPTION)
GLVAULT_API_TOKEN = os.getenv("GLVAULT_API_TOKEN", UNSET_OPTION)

LOG = logging.getLogger(__name__)

pretty_json = functools.partial(json.dumps, ensure_ascii=True, indent=2)

# Argument parser settings and associated validators / utilities


def endpoint_type(value: str) -> str:
    """Validates an endpoint URL"""
    if value == UNSET_OPTION:
        raise argparse.ArgumentTypeError("You must provide an endpoint URL.")
    parts = urlparse(value)
    *ignored, ctype, cid = parts.path.split("/")
    if ctype not in {"projects", "groups"}:
        raise argparse.ArgumentTypeError(
            f"The container type must be 'projects' or 'groups' ('{ctype}' found)."
        )
    try:
        int(cid)
    except ValueError:
        raise argparse.ArgumentTypeError(f"The project or group id must be an integer ('{cid}')'")
    return value


def token_type(value: str) -> str:
    """Lazily validates a Gitlab API token"""
    if value == UNSET_OPTION:
        raise argparse.ArgumentTypeError("You must provide a Gitlab token with API usage granted.")
    return value


class Format(enum.IntEnum):
    """Possible output formats"""

    DEFAULT = 1
    HUMAN = 2
    JSON = 3


def make_arg_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=__description__)
    add_arg = parser.add_argument

    # Global options
    add_arg("--version", action="version", version=__version__)
    add_arg(
        "-e",
        "--endpoint",
        default=GLVAULT_ENDPOINT,
        type=endpoint_type,
        help="(Required) Provide an endpoint for a project or a group."
        " You may provide a default value with GLVAULT_ENDPOINT environment variable."
        f" Its actual default value is '{GLVAULT_ENDPOINT}'"
        " An endpoint URL must be in form 'http://mygitlab.mydomain.tld/projects/122' for project id 122 variables,"
        " or 'http://mygilab.mydomain.tld/groups/32' for group id 32 variables.",
    )
    add_arg(
        "-t",
        "--api-token",
        default=GLVAULT_API_TOKEN,
        type=token_type,
        help="(Required) Provide an API token built with credentials that grant r/w access to the"
        " CI/CD variables of the above selected group or project."
        " You may provide a default value with GLVAULT_API_TOKEN environment variable."
        f" Its actual value is '{GLVAULT_API_TOKEN}'",
    )
    format_parser = parser.add_mutually_exclusive_group()
    add_arg = format_parser.add_argument
    add_arg(
        "--human", action="store_const", dest="format", const=Format.HUMAN, default=Format.DEFAULT
    )
    add_arg(
        "--json", action="store_const", dest="format", const=Format.JSON, default=Format.DEFAULT
    )
    return parser


class Application:
    """The main Application class

    Args:
        argv: The command line as a list as ``sys.argv``
    """

    def __init__(self, argv):
        ap: argparse.ArgumentParser = make_arg_parser()
        subparsers = ap.add_subparsers(help="Available commands")
        for cmd_class in all_commands:
            cmd = cmd_class(subparsers)
        self.args = ap.parse_args(args=argv[1:])
        if not hasattr(self.args, "func"):
            ap.print_help()
            sys.exit(0)

    def run(self):
        # FIXME: Replace below sample with your code
        glvault = GLVault(self.args.endpoint, self.args.api_token)
        self.args.func(self.args, glvault)


class CommandBase(abc.ABC):
    """Base class with abstract attrs/methods for all commands"""

    @property
    @abc.abstractmethod
    def name(self):
        """Provide a subcommand name"""
        return NotImplemented

    @property
    @abc.abstractmethod
    def help(self):
        """Provide a help text"""
        return NotImplemented

    def __init__(self, subparsers):
        cmd_parser = subparsers.add_parser(self.name, help=self.help)
        cmd_parser.set_defaults(func=self.run)
        self.init(cmd_parser)

    def init(self, cmd_parser: argparse.ArgumentParser):
        """Post initialization, i.e add subclass specific options, etc.
        Does nothing by default

        Args:
            cmd_parser: parser for actual command for additional params and options
        """
        return

    @abc.abstractmethod
    def run(self, args: argparse.Namespace, glvault: "GLVault"):
        """Execution body of the command"""
        return NotImplemented


class ListCommand(CommandBase):
    name = "list"
    help = "Show a list of all available variables"

    def run(self, args, glvault):
        out = glvault.list()
        if args.format == Format.HUMAN:
            for count, variable in enumerate(out):
                if count > 0:
                    print("------------------")
                print(variable["key"])
                print("Value:            ", variable["value"])
                print("Protected:        ", variable["protected"])
                print("Environment scope:", variable["environment_scope"])
        elif args.format == Format.JSON:
            print(pretty_json(out))
        else:  # args.format == Format.DEfAULT
            for variable in out:
                print(f"{variable['key']}=\"{variable['value']}\"")


class ShowCommand(CommandBase):
    name = "show"
    help = "Shows a single variable"

    def init(self, cmd_parser):
        cmd_parser.add_argument("name", help="Name of the variable")

    def run(self, args, glvault):
        out = glvault.show(args.name)
        if args.format == Format.HUMAN:
            print(out["key"])
            print("Value:            ", out["value"])
            print("Protected:        ", out["protected"])
            print("Environment scope:", out["environment_scope"])
        elif args.format == Format.JSON:
            print(pretty_json(out))
        else:  # args.format == Format.DEfAULT
            print(f"{out['key']}=\"{out['value']}\"")


class NewCommand(CommandBase):
    name = "new"
    help = "Create a new variable"

    def init(self, cmd_parser):
        cmd_parser.add_argument(
            "--protected", action="store_true", help="create a protected variable"
        )
        cmd_parser.add_argument(
            "--env-scope", default="*", help='environment scope (default is "*")'
        )
        cmd_parser.add_argument("name", help="name of the new variable")
        cmd_parser.add_argument("value", help="initial value for this new variable")

    def run(self, args, glvault):
        out = ""
        try:
            out = glvault.new(
                args.name, args.value, protected=args.protected, environment_scope=args.env_scope
            )
        except HTTPError as exc:
            LOG.exception(f'Could not create new variable named "{args.name}"')
        print(pretty_json(out))


class UpdateCommand(CommandBase):
    name = "update"
    help = "Updates an existing variable"

    def init(self, cmd_parser):
        cmd_parser.add_argument(
            "--protected", action="store_true", help="protect the updated variable"
        )
        cmd_parser.add_argument(
            "--env-scope", default="*", help='environment scope (default is "*")'
        )
        cmd_parser.add_argument("name", help="name of the new variable")
        cmd_parser.add_argument("value", help="initial value for this new variable")

    def run(self, args, glvault):
        out = ""
        try:
            out = glvault.update(
                args.name, args.value, protected=args.protected, environment_scope=args.env_scope
            )
        except HTTPError as exc:
            LOG.exception(f'Could not update variable named "{args.name}"')
        print(pretty_json(out))


class RemoveCommand(CommandBase):
    name = "remove"
    help = "Removes an existing variable"

    def init(self, cmd_parser):
        cmd_parser.add_argument("name", help="name of the new variable")

    def run(self, args, glvault):
        out = glvault.remove(args.name)
        return pretty_json(out)


# All should be subclasses od CommandBase
all_commands = (ListCommand, ShowCommand, NewCommand, UpdateCommand, RemoveCommand)

#  Gitlab API wrappers and utilities


def error_in_response(response) -> bool:
    """True if the HTTP response status code notifies an error"""
    return response.getcode() >= 400


page_link_re = re.compile(r'\s*<(.*)>;\s*rel="(.*)"\s*')


def parse_pagination(response):
    """Parse pagination headers.
    See: https://docs.gitlab.com/ee/api/#pagination-link-header
    """
    link_header = response.headers.get("Link")
    out = {}
    if link_header:
        links = link_header.split(",")
        for link in links:
            result = page_link_re.match(link)
            url = result.group(1).strip()
            which = result.group(2).strip()
            out[which] = url
    return out


class GLVault:
    """API wrapper for Gitlab group/project variables access"""

    def __init__(self, endpoint: str, token: str) -> None:
        parts = list(urlparse(endpoint))
        parts[2] = "/api/v4" + parts[2]
        if not parts[2].endswith("/"):
            parts[2] = parts[2] + "/"
        self.api_base = urlunparse(parts)
        self.token = token

    def _build_request(self, operation: str) -> Request:
        """Common request featured object for API"""
        request = Request(urljoin(self.api_base, operation), headers={"PRIVATE-TOKEN": self.token})
        return request

    def list(self) -> list:
        """Lists all variables.
        Projects: https://docs.gitlab.com/ee/api/project_level_variables.html#list-project-variables
        Groups: https://docs.gitlab.com/ee/api/group_level_variables.html#list-group-variables
        """
        have_next = True
        operation = "variables"
        out = []
        while have_next:
            request = self._build_request(operation)
            with urlopen(request) as response:
                result = json.load(response)
            out.extend(result)
            pagination = parse_pagination(response)
            next_url = pagination.get("next")
            if next_url:
                qs = urlparse(next_url).query
                operation = "variables?" + qs
            else:
                have_next = False
        return out

    def show(self, name: str) -> dict:
        """Shows a variable
        Project: https://docs.gitlab.com/ee/api/project_level_variables.html#show-variable-details
        Group: https://docs.gitlab.com/ee/api/group_level_variables.html#show-variable-details
        """
        request = self._build_request("variables/" + name.strip())
        with urlopen(request) as response:
            result = json.load(response)
        return result

    def new(
        self, name: str, value: str, protected: bool = False, environment_scope: str = "*"
    ) -> dict:
        """Creates a variable.
        Project: https://docs.gitlab.com/ee/api/group_level_variables.html#list-group-variables
        Group: https://docs.gitlab.com/ee/api/group_level_variables.html#create-variable
        """
        request = self._build_request("variables")
        request.method = "POST"
        payload = {
            "key": name,
            "value": str(value),
            "protected": str(protected).lower(),
            "environment_scope": environment_scope,
        }
        payload = urlencode(payload).encode()
        request.data = payload

        with urlopen(request) as response:
            result = json.load(response)
        return result

    def update(
        self, name: str, value: str, protected: bool = False, environment_scope: str = "*"
    ) -> dict:
        """Updates an existing variable
        Project: https://docs.gitlab.com/ee/api/project_level_variables.html#update-variable
        Group: https://docs.gitlab.com/ee/api/project_level_variables.html#update-variable
        """
        request = self._build_request("variables/" + name.strip())
        request.method = "PUT"
        payload = {
            "value": str(value),
            "protected": str(protected).lower(),
            "environment_scope": environment_scope,
        }
        payload = urlencode(payload).encode()
        request.data = payload
        with urlopen(request) as response:
            result = json.load(response)
        return result

    def remove(self, name: str):
        """Removes an existing variable
        Project: https://docs.gitlab.com/ee/api/project_level_variables.html#remove-variable
        Group: https://docs.gitlab.com/ee/api/group_level_variables.html#remove-variable
        """
        request = self._build_request("variables/" + name.strip())
        request.method = "DELETE"
        with urlopen(request) as response:
            result = response
        return result


#  Entry point


def main(argv=sys.argv):
    exit_code = 1
    try:
        app = Application(argv)
        app.run()
        exit_code = 0
    except KeyboardInterrupt:
        exit_code = 0
    except Exception as exc:
        LOG.exception(str(exc))
    sys.exit(exit_code)


if __name__ == "__main__":
    main()
